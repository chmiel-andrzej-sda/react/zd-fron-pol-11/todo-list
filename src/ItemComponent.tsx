import Chip from '@mui/material/Chip';
import { TodoItem } from './entity/TodoItem';
import './ItemComponent.css';
import Button from '@mui/material/Button';

interface ItemComponentProps {
    readonly item: TodoItem;
	readonly onDone: () => void;
	readonly onUndone: () => void;
}

export function ItemComponent(props: ItemComponentProps): JSX.Element {
	function priorityColor(priority: number): 'error' | 'warning' | 'success' | 'default' {
		switch(priority) {
			case 1:
				return 'error';
			case 2:
				return 'warning';
			case 3:
				return 'success';
			default:
				return 'default';
		}
	}

    function renderButton(): JSX.Element {
		if (props.item.done) {
			return <Button variant="contained" onClick={props.onUndone}>UNDONE</Button>;
		}
		return <Button variant="contained" onClick={props.onDone}>DONE</Button>;
    }

    return <div>
		<Chip
			color={priorityColor(props.item.priority)}
			label={props.item.priority}
			variant="filled"
			size="small"
		/>
		<span className={`todo-item-text ${props.item.done ? 'done' : ''}`}>
			{props.item.text}
		</span>
		<span className={`todo-item-date ${props.item.date < new Date() ? 'overdue' : ''}`}>
			{props.item.date.toUTCString()}
		</span>
		{renderButton()}
		<span>{props.item.done?.toUTCString()}</span>
	</div>;
}
