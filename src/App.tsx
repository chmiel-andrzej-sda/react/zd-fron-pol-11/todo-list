import React from 'react';
import { ListComponent } from './ListComponent';
import { TodoItem } from './entity/TodoItem';
import { NewTodoComponent } from './NewTodoComponent';

export function App(): JSX.Element {
	const [items, setItems] = React.useState<TodoItem[]>([
		{
			text: 'Past test item 1',
			date: new Date("2022-11-11 11:11:11"),
			priority: 1
		},
		{
			text: 'Future test item 2',
			done: new Date("2022-12-12 10:12:14"),
			date: new Date("2033-11-11 11:11:11"),
			priority: 2
		}
	]);

	function handleDone(item: TodoItem): void { // (item: TodoItem) => void
		const index: number = items.indexOf(item);
		const copy: TodoItem = {
			...item,
			done: new Date()
		};
		items[index] = copy;
		setItems([...items]);
	}

	function handleUndone(item: TodoItem): void {
		const index: number = items.indexOf(item);
		const copy: TodoItem = {
			...item,
			done: undefined
		};
		items[index] = copy;
		setItems([...items]);
	}

	function handleAdd(item: TodoItem): void {
		console.log(item);
		setItems([...items, item]);
	}

    return <div className="App">
		<ListComponent items={items} onDone={handleDone} onUndone={handleUndone}/>
		<NewTodoComponent onAdd={handleAdd}/>
    </div>;
}
