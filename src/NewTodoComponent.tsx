import React from 'react'
import { TodoItem } from './entity/TodoItem';
import Button from '@mui/material/Button';
import { TextField } from '@mui/material';
import { PrioritySelect } from './PrioritySelect';

interface NewTodoComponentProps {
	readonly onAdd: (item: TodoItem) => void;
}

export function NewTodoComponent(props: NewTodoComponentProps): JSX.Element {
	const [text, setText] = React.useState<string>('');
	const [date, setDate] = React.useState<string>('');
	const [priority, setPriority] = React.useState<number>(2);

	function handleChange(event: React.ChangeEvent<HTMLTextAreaElement>): void {
		setText(event.target.value);
	}

	function handleDateChange(event: React.ChangeEvent<HTMLInputElement>): void { 
		setDate(event.target.value);
	}

	function handleClick(): void {
		if (text && date && new Date() < new Date(date)) {
			props.onAdd({
				text,
				date: new Date(date),
				priority
			});
		}
		setText('');
		setDate('');
		setPriority(2);
	}

	function handlePriorityChange(priority: number): void {
		setPriority(priority);
	}

	return <div>
		<TextField
			multiline
			onChange={handleChange}
			value={text}
			minRows={4}
			maxRows={10}
			fullWidth
		/>
		<input type="datetime-local" onChange={handleDateChange} value={date}/>
		<br/>
		<PrioritySelect onChange={handlePriorityChange} priority={priority}/>
		<br/>
		<Button
			onClick={handleClick}
			disabled={!text || !date || new Date() > new Date(date)}
			variant="outlined"
		>Add</Button>
	</div>;
}
