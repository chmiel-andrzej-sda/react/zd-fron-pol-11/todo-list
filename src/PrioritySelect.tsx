import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import Select, { SelectChangeEvent } from "@mui/material/Select";

interface PrioritySelectProps {
	readonly priority: number;
	readonly onChange: (priority: number) => void;
}

export function PrioritySelect(props: PrioritySelectProps): JSX.Element {
	function handleChange(event: SelectChangeEvent<string>): void {
		props.onChange(+event.target.value);
	}

	return <>
		<InputLabel>Select priority</InputLabel>
		<Select
			onChange={handleChange}
			label="Priority"
			value={`${props.priority}`}
		>
			<MenuItem value={3}>Minor</MenuItem>
			<MenuItem value={2}>Medium</MenuItem>
			<MenuItem value={1}>High</MenuItem>
		</Select>
	</>;
}
