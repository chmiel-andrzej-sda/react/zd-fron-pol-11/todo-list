import List from '@mui/material/List';
import { TodoItem } from './entity/TodoItem';
import { ItemComponent } from './ItemComponent';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';

interface ListComponentProps {
	readonly items: TodoItem[];
	readonly onDone: (item: TodoItem) => void;
	readonly onUndone: (item: TodoItem) => void;
}

export function ListComponent(props: ListComponentProps) {
	function renderItem(item: TodoItem, index: number): JSX.Element {
		return <ListItem disablePadding key={index}>
			<ListItemButton>
				<ListItemText
					primary={<ItemComponent
						item={item}
						onDone={(): void => props.onDone(item)}
						onUndone={(): void => props.onUndone(item)}
					/>}
				/>
			</ListItemButton>
		</ListItem>;
	}

	return <div>
		<nav aria-label="secondary mailbox folders">
			<List>
				{props.items.map(renderItem)}
			</List>
		</nav>
	</div>;
}
