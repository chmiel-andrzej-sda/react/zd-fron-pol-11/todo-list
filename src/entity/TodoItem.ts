export interface TodoItem {
    readonly text: string;
    readonly date: Date;
    readonly done?: Date;
    readonly priority: number;
}
